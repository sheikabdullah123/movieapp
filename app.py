from distutils.log import debug
from msilib.schema import File
from flask import Flask, render_template, request, jsonify
from flask_mysqldb import MySQL
import yaml
import json
import logging

app = Flask(__name__)

logging.basicConfig(filename='record.log', level=logging.DEBUG, format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')
db = yaml.safe_load(open('db.yaml'))
app.config['MYSQL_HOST'] = db['mysql_host']
app.config['MYSQL_USER'] = db['mysql_user']
app.config['MYSQL_PASSWORD'] = db['mysql_password']
app.config['MYSQL_DB'] = db['mysql_db']

mysql = MySQL(app)

@app.route('/')
def loginpage():
    return render_template('login.html')

@app.route('/adminhome')
def adminpage():
    return render_template('adminhome.html')

@app.route('/memberhome')
def memberpage():
    return render_template('memberhome.html')



@app.route('/movieslist', methods=['GET'])
def index():
    if request.method == 'GET':
        cur = mysql.connection.cursor()
        
        cur.execute("select * from moviedetails")
        result_new = cur.fetchall()
        app.logger.info('Fetched the movies details !!!!')
        cur.close()
        return jsonify({'moviedetails': result_new})

@app.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        result_1 = request.form['typeofuser']
        print(result_1)
        if  result_1== 'admin':
            username = request.form['username']
            
            password = request.form['password']
            cur = mysql.connection.cursor()
            result=cur.execute("SELECT * from userdetails where username=%s and password=%s", (username, password))
            app.logger.info('Fetching the username and password of the admin')

            cur.close()
            if result > 0:
                app.logger.info("Admin User Exists")
                return jsonify({'userexists': "yes", 'admin':"yes", 'username':username})
            else:
                app.logger.info("Admin user does not exists")
                return jsonify({'userexists': "no"})
        elif result_1=='member':
            username = request.form['username']
            password = request.form['password']
            cur = mysql.connection.cursor()
            result=cur.execute("SELECT * from userdetails where username=%s and password=%s", (username, password))
            app.logger.info("Fetched username and password of Member")
            cur.close()
            if result > 0:
                app.logger.info("Member user exists")
                return jsonify({'userexists': "yes", 'admin':"no", 'username':username})
            else:
                app.logger.info("Member user does not exists")
                return jsonify({'userexists': "no"})

@app.route('/search', methods=['GET', 'POST'])
def search():
    if request.method == 'POST':
        search_type = request.form['searchtype']
        

        if search_type == 'moviename':
            cur = mysql.connection.cursor()
            type = 'Movie'
            search = request.form.get('searchchar')
            searchchar = '%'+search+'%'
            result = cur.execute("SELECT * from moviedetails where type=%s and title like %s limit 5", (type, searchchar))
            app.logger.info("Fetching the movie details based on title.....")
            dict_val = {}
            dict_list = []
            if result > 0:
                search_result = cur.fetchall()
                for i in search_result:
                    showid, typeof, title, directors, cast, country, releaseyear, rating, duration, listedin, description = i
                    dict_val = {'showid': showid, 'typeof':typeof, 'title':title, 'directors':directors, 'cast':cast, 'country': country, 'releaseyear':releaseyear, 'rating': rating, 'duration':duration, 'listedin': listedin, 'description':description}
                    dict_list.append(dict_val)
                app.logger.info("Fetched movie details based on cast")
                cur.close()
                return jsonify({'MovieDetails': dict_list})
            else:
                cur.close()
                app.logger.info("Movie details does not exist based on title")
                return jsonify({'0':"No data found"})

        elif search_type == 'actors':
            cur = mysql.connection.cursor()
            actor = request.form['searchchar']
            searchchar = '%'+actor+'%'
            result = cur.execute("SELECT distinct * from moviedetails where cast like %s limit 5", (searchchar,))
            app.logger.info("Fetching movie details based on cast....")
            dict_val = {}
            dict_list = []
            if result > 0:
                search_result = cur.fetchall()
                for i in search_result:
                    showid, typeof, title, directors, cast, country, releaseyear, rating, duration, listedin, description = i
                    dict_val = {'showid': showid, 'typeof':typeof, 'title':title, 'directors':directors, 'cast':cast, 'country': country, 'releaseyear':releaseyear, 'rating': rating, 'duration':duration, 'listedin': listedin, 'description':description}
                    dict_list.append(dict_val)
                app.logger.info("Fetched movie details based on cast")
                cur.close()
                return jsonify({'MovieDetails': dict_list})
            else:
                cur.close()
                app.logger.info("Movie details does not exists based on cast")
                return jsonify({'0': "No data found"})

        elif search_type == 'year':
            cur = mysql.connection.cursor()
            year = request.form['searchchar']
            searchchar = '%'+year+'%'
            result = cur.execute("SELECT * from moviedetails where release_year like %s limit 5", (searchchar,))
            app.logger.info("Fetching movie details based on release year....")
            dict_val = {}
            dict_list = []
            if result > 0:
                search_result = cur.fetchall()
                for i in search_result:
                    showid, typeof, title, directors, cast, country, releaseyear, rating, duration, listedin, description = i
                    dict_val = {'showid': showid, 'typeof':typeof, 'title':title, 'directors':directors, 'cast':cast, 'country': country, 'releaseyear':releaseyear, 'rating': rating, 'duration':duration, 'listedin': listedin, 'description':description}
                    dict_list.append(dict_val)
                app.logger.info("Fetched movie details based on cast")
                cur.close()
                return jsonify({'MovieDetails': dict_list})
            else:
                cur.close()
                app.logger.info("Movie details does not exists based on release year")
                return jsonify({'0': "No data found"})

        elif search_type == 'directors':
            cur = mysql.connection.cursor()
            director = request.form['searchchar']
            searchchar = '%'+director+'%'
            result = cur.execute("SELECT * from moviedetails where director like %s limit 5", (searchchar,))
            app.logger.info("Fetching movie details based on director....")
            dict_val = {}
            dict_list = []
            if result > 0:
                search_result = cur.fetchall()
                for i in search_result:
                    showid, typeof, title, directors, cast, country, releaseyear, rating, duration, listedin, description = i
                    dict_val = {'showid': showid, 'typeof':typeof, 'title':title, 'directors':directors, 'cast':cast, 'country': country, 'releaseyear':releaseyear, 'rating': rating, 'duration':duration, 'listedin': listedin, 'description':description}
                    dict_list.append(dict_val)
                app.logger.info("Fetched movie details based on cast")
                cur.close()
                return jsonify({'MovieDetails': dict_list})
            else:
                cur.close()
                app.logger.info("Movie details does not exists based on directors")
                return jsonify({'0': "No data found"})


@app.route('/movie_detail', methods=['GET'])
def moviedetail():
    if request.method == 'GET':
        cur = mysql.connection.cursor()
        show_id = request.args.get('show_id')
        result = cur.execute("SELECT * from moviedetails where show_id =%s", (show_id,))
        app.logger.info("Fetching movie details based on show id")
        if result >0:
            search_result = cur.fetchall()
            app.logger.info("Fetched movie details based on show id")
            cur.close()
            return jsonify({'MovieDetails': search_result})
        else:
            cur.close()
            app.logger.info("Movie details does not exists based on show id")
            return jsonify({'0': "No data found"})

@app.route('/create_movie', methods=['POST'])
def addmovie():
    if request.method == 'POST':
        user_type = request.form['user-type']
        if user_type == 'admin':
            cur = mysql.connection.cursor()
            conn = mysql.connection
            show_id = request.form['show-id']
            type = request.form['type']
            title = request.form['title']
            director = request.form['director']
            cast = request.form['cast']
            country = request.form['country']
            release_year = request.form['release-year']
            rating = request.form['rating']
            duration = request.form['duration']
            listed_in = request.form['listed-in']
            description = request.form['description']
            result = cur.execute("SELECT title from moviedetails where title=%s", (title,))
            if result >0:
                app.logger.info("Fetching title based on title....")
                return jsonify({'0': "Movie title Already Exists"})
            else:
                movie_added = cur.execute("INSERT INTO moviedetails values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (show_id, type, title, director, cast, country, release_year, rating, duration, listed_in, description))
                mysql.connection.commit()
                if movie_added > 0:
                    cur.close()
                    app.logger.info("Movie added successfully")
                    return jsonify({'1': "Movie Added Successfully"})
                else:
                    cur.close()
                    app.logger.info("Issue with movie addition")
                    return jsonify({'2': "Issue with Movie addition"})
        else:
            app.logger.info("Only Admin have rights to add a movie")
            return jsonify({'0': "Only Admin have rights to add a Movie"})

@app.route('/delete_movie', methods=['POST'])
def deletemovie():
    if request.method == 'POST':
        # user_type = request.args.get('user_type')
        # if user_type == 'admin':
        cur = mysql.connection.cursor()
        conn = mysql.connection
        showid = request.get_data('showid').decode('ascii')
        result = cur.execute("SELECT title from moviedetails where show_id=%s", (showid,))
        app.logger.info("Fetching title....")
        if result > 0:
            movie_deleted = cur.execute("DELETE from moviedetails where show_id=%s", (showid,))
            mysql.connection.commit()
            if movie_deleted > 0:
                cur.close()
                app.logger.info("Deleted a movie record successfully")
                
                return jsonify({'1': "Movie Deleted Successfully"})
            else:
                cur.close()
                app.logger.info("Issue with movie deletion")
                return jsonify({'0': "Issue with Movie deletion"})
        else:
            app.logger.info("Movie not found try again with other title")
            return jsonify({'0': "Movie not found try again with other title"})

@app.route('/update_movie', methods=['POST'])
def updatemovie():
    if request.method == 'POST':
        user_type = request.args.get('user_type')
        if user_type == 'admin':
            cur = mysql.connection.cursor()
            conn = mysql.connection
            title = request.args.get('title')
            result = cur.execute("SELECT title from moviedetails where title=%s", (title,))
            app.logger.info("Fetching title...")
            if result > 0:
                update_type = request.args.get('update_type')
                value = request.args.get('value')
                if update_type == 'type':
                    status = cur.execute("UPDATE moviedetails SET type=%s where title=%s", (value, title))
                    app.logger.info("Updaing type of the movie")
                elif update_type == 'title':
                    status = cur.execute("UPDATE moviedetails SET title=%s where title=%s", (value, title))
                    app.logger.info("Updating title of the movie")
                elif update_type == 'director':
                    status = cur.execute("UPDATE moviedetails SET director=%s where title=%s", (value, title))
                    app.logger.info("Updating director of the movie")
                elif update_type == 'cast':
                    status = cur.execute("UPDATE moviedetails SET cast=%s where title=%s", (value, title))
                    app.logger.info("Updating cast of the movie")
                elif update_type == 'country':
                    status = cur.execute("UPDATE moviedetails SET country=%s where title=%s", (value, title))
                    app.logger.info("Updating country of the movie")
                elif update_type == 'release_year':
                    status = cur.execute("UPDATE moviedetails SET release_year=%s where title=%s", (value, title))
                    app.logger.info("Updating release year of the movie")
                elif update_type == 'rating':
                    status = cur.execute("UPDATE moviedetails SET rating=%s where title=%s", (value, title))
                    app.logger.info("Updating rating of the movie")
                elif update_type == 'duration':
                    status = cur.execute("UPDATE moviedetails SET duration=%s where title=%s", (value, title))
                    app.logger.info("Updating duration of the movie")
                elif update_type == 'listed_in':
                    status = cur.execute("UPDATE moviedetails SET listed_in=%s where title=%s", (value, title))
                    app.logger.info("Updating listed in of the movie")
                elif update_type == 'description':
                    status = cur.execute("UPDATE moviedetails SET description=%s where title=%s", (value, title))
                    app.logger.info("Updating description of the movie")
                else: 
                    app.logger.info("Specified column does not exists")
                    return jsonify({'1': "Specified column does not exists"})

                mysql.connection.commit()
                if status > 0:
                    cur.close()
                    app.logger.info("Movie updated Successfully")
                    return jsonify({'1': "Movie updated Successfully"})
                else:
                    cur.close()
                    app.logger.info("Issue with movie updation")
                    return jsonify({'0': "Issue with movie updation"})
            else:
                cur.close()
                app.logger.info("Movie does not exists try with other title")
                return jsonify({'0': "Movie does not exists try with other title"})
        else:
            app.logger.info("Only Admin have rights to update a movie record")
            return jsonify({'1': "Only Admin have rights to update a movie record"})



if __name__ == '__main__':
    
    app.run(debug=True)
    


